FROM node:10

ADD . /app
WORKDIR /app

RUN npm install

ENTRYPOINT ["npm", "start"]
