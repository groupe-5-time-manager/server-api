# Time-Manager

## Development environment 
To start dev localy, use the following command and open your browser at ```http://localhost:4000```
```
> docker-compose up --build
```

To clean docker containers and volumes run :
```
> docker-compose down
> sudo docker container prune -f
> sudo docker volume prune -f
> docker-compose up --build -d
```

## Git Workflow

### Branch Naming Guidelines
Convention for branch naming are based as follow : ```scope/the-feature```.
#### Samples:
```
api/working-time
api/clocks
ihm/user
ihm/chart-manager
```

### Commit Message Guidelines
Each commit message consists of a header, a body and a footer. The header has a special format that includes a type, a scope and a subject:
```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```
#### Samples:
```
feat(workingTime): changing HTTP status codes
docs(changelog): update changelog to beta.5
fix(release): need to depend on latest rxjs and zone.js
```
#### Type
Must be one of the following:

- __build__: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- __ci__: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
- __docs__: Documentation only changes
- __feat__: A new feature
- __fix__: A bug fix
- __perf__: A code change that improves performance
- __refactor__: A code change that neither fixes a bug nor adds a feature
- __style__: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- __test__: Adding missing tests or correcting existing tests
