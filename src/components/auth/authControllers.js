const bcrypt = require('bcrypt');
const crypto = require('crypto');
const errors = require('../../errors');
const database = require('../../database');
const config = require('../../config');
const { generateJsonWebToken, verifyJsonWebToken } = require('../../utils');

async function authMiddleware(req) {
  try {
    const token = req.cookies['token']
    const { xsrfToken, sub } = await verifyJsonWebToken(token)

    const sentXsrfToken = req.get('X-CSRF-TOKEN')
    if (!sentXsrfToken)
      throw new errors.AuthError('Missing header')
    if (sentXsrfToken != xsrfToken)
      throw new errors.AuthError('Bad xsrf token')
    const user = await database.user.findByPk(sub);
    if (!user) {
      throw new errors.NotFoundError('AUTH01', 'User');
    }
    return { user };
  } catch (err) {
    return { err };
  }
}

async function userMiddleware(req, res, next) {
  try {
    const { user, err } = await authMiddleware(req);
    if (err) {
      throw err;
    }
    if (!user) {
      throw new errors.NotFoundError('AUTH01', 'User');
    }
    res.locals.user = user;
    return next();
  } catch (err) {
    return next(err);
  }
}

async function managerMiddleware(req, res, next) {
  try {
    const { user, err } = await authMiddleware(req);
    if (err) {
      throw err;
    }
    if (!user) {
      throw new errors.NotFoundError('USER01', 'User');
    }
    if (user.roleId > 2) {
      throw new errors.AuthError('Your role does not allow you to access this resource');
    }
    res.locals.user = user;
    return next();
  } catch (err) {
    return next(err);
  }
}

async function adminMiddleware(req, res, next) {
  try {
    const { user, err } = await authMiddleware(req);
    if (err) {
      throw err;
    }
    if (!user) {
      throw new errors.NotFoundError('USER01', 'User');
    }
    if (user.roleId > 1) {
      throw new errors.AuthError('Your role does not allow you to access this resource');
    }
    res.locals.user = user;
    return next();
  } catch (err) {
    return next(err);
  }
}

async function signup(params) {
  try {
    let user = await database.user.findOne({ where: { email: params.email } });
    if (user)
      throw new errors.UserAlreadyExistsError(params.email);
    const password = bcrypt.hashSync(params.password + config.salt, config.saltRounds);
    params.password = password;
    params.roleId = (await database.role.findOne({where: { label: 'user' }})).id;
    delete params.role;
    user = await database.user.create(params);
    return user;
  } catch (err) {
    throw err;
  }
}

async function signin(params) {
  const user = await database.user.findOne({where: { email: params.email }});
  if (!user) {
    throw new errors.NotFoundError('USER07', 'User');
  }

  try {
    const match = bcrypt.compareSync(params.password + config.salt, user.password);
    if (match) {
      const xsrfToken = crypto.randomBytes(48).toString('hex');
      const jwt = generateJsonWebToken('gandalf', user.id, { xsrfToken });
      return { jwt, xsrfToken, userId: user.id, roleId: user.roleId }
    }
  } catch (e) {
    throw new errors.InternalError('AUTH03', e.message);
  }
  throw new errors.AuthError('Bad password');
}

module.exports = {
  userMiddleware,
  managerMiddleware,
  adminMiddleware,
  signup,
  signin,
};
