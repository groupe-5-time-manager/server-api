const express = require('express');
const httpStatus = require('http-status-codes');
const uuid = require('uuid/v4');

const errors = require('../../errors');

const authControllers = require('./authControllers');
const authValidators = require('./authValidators');

const router = express.Router();

router.post('/signup', async (req, res, next) => {
  try {
    const params = authValidators.validateSignup(req);
    const auth = await authControllers.signup({ id: uuid(), ...params });
    const signinParams = { email: params.email, password: params.password };
    const { jwt, xsrfToken, userId, roleId } = await authControllers.signin(signinParams);
    res.cookie('token', jwt, { httpOnly: true });
    return res.status(httpStatus.CREATED).send({ xsrfToken, userId, roleId });
  } catch (err) {
    return next(err);
  }
})

router.post('/signin', async (req, res, next) => {
  try {
    const params = authValidators.validateSignin(req);
    const { jwt, xsrfToken, userId, roleId } = await authControllers.signin(params);
    res.cookie('token', jwt, {httpOnly: true});
    return res.status(httpStatus.OK).send({ xsrfToken, userId, roleId });
  } catch (err) {
    return next(err);
  }
})

module.exports = router;
