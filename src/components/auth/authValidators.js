const validate = require('validate.js');
const { RequestParametersError } = require('../../errors');

const signupConstraints = {
  firstname: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  lastname: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  email: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  password: {
    type: 'string',
    presence: { allowEmpty: true }
  },
};

function validateSignup(req) {
  const errors = validate(req.body, signupConstraints);
  if (errors) {
    throw new RequestParametersError(errors);
  }
  return req.body;
}

const signinConstraints = {
  email: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  password: {
    type: 'string',
    presence: { allowEmpty: true }
  },
};

function validateSignin(req) {
  const errors = validate(req.body, signinConstraints);
  if (errors) {
    throw new RequestParametersError(errors);
  }
  return req.body;
}

module.exports = {
  validateSignup,
  validateSignin,
};
