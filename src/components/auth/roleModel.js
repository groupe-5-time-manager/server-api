const DataTypes = require('sequelize');

const roles = ['admin', 'manager', 'user']

const name = 'role';

const fields = {
  id: {
    notEmpty: true,
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  label: {
    notEmpty: true,
    type: DataTypes.ENUM(roles),
    unique: true,
  }
};

module.exports = { roles, name, fields };
