const uuid = require('uuid/v4');
const errors = require('../../errors');
const database = require('../../database');

const getClocks = async (userId) => {
  try {
    const clocks = await database.clock.findAll({ where: { userId } });
    return clocks;
  } catch (err) {
    throw new errors.DatabaseFindError('CLOCK01', 'Clock');
  }
}

const createClock = async (userId, params) => {
  try {
    const lastClocks = await database.clock.findAll({ limit: 1, order: [['createdAt', 'DESC']] });

    if (lastClocks.length > 0 && lastClocks[0].status === params.status) {
      console.log("AAA")
      throw new Error();
    }
    if (lastClocks.length === 0 && !params.status) {
      console.log("BBB")
      throw new Error();
    }
    const clock = await database.clock.create({
      id: uuid(),
      ...params,
      userId
    });
    if (!params.status) {
      console.log("CCC")
      const start = lastClocks[0].time;
      const end = clock.time;
      await database.workingTime.create({ id: uuid(), start, end, userId });
    }
    console.log("DDD")
    return clock;
  } catch (err) {
    throw new errors.DatabaseCreationError('CLOCK02', 'Clock creation failed');
  }
}

module.exports = {
  getClocks,
  createClock
 };
