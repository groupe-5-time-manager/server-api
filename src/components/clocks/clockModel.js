const DataTypes = require('sequelize');

const name = 'clock';

const fields = {
  id: {
    notEmpty: true,
    type: DataTypes.STRING,
    primaryKey: true,
  },
  time: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  status: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
  },
};

module.exports = { name, fields };
