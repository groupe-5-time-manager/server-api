const express = require('express');
const httpStatus = require('http-status-codes');

const clockControllers = require('./clockControllers');
const clockValidators = require('./clockValidators');

const router = express.Router();

/**
 * @api {get} /api/clocks/:userId Get all clocks for one user
 * @apiName GetClocks
 * @apiGroup Clock
 *
 * @apiParam {String} userId User id
 *
 * @apiSuccess {Object[]} clocks
 * @apiSuccess {String} clock.id Clock id
 * @apiSuccess {String} clock.time Clock time
 * @apiSuccess {Boolean} clock.status Clock in or out
 */
router.get('/', async (req, res, next) => {
  try {
    const result = await clockControllers.getClocks(res.locals.user.id);
    return res.status(httpStatus.OK).send(result);
  } catch (err) {
    return next(err);
  }
});

/**
 * @api {post} /api/clocks/:userId Create a clock for one user
 * @apiName PostClocks
 * @apiGroup Clock
 *
 * @apiParam {String} userId User id
 * @apiParam {String} time The current time
 * @apiParam {Boolean} status True for in / false for out
 *
 * @apiSuccess {String} id Clock id
 * @apiSuccess {String} time Clock time
 * @apiSuccess {Boolean} status Clock in or out
 */
router.post('/', async (req, res, next) => {
  try {
    const params = clockValidators.validateCreateClock(req);
    const result = await clockControllers.createClock(res.locals.user.id, params);
    return res.status(httpStatus.CREATED).send(result);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
