const validate = require('validate.js');
const { RequestParametersError } = require('../../errors');

const createClockConstraints = {
  time: {
    type: 'string',
    presence: {allowEmpty: false}
  },
  status: {
    type: 'boolean',
    presence: {allowEmpty: false}
  },
};

function validateCreateClock(req) {
  const errors = validate(req.body, createClockConstraints);
  if (errors) {
    throw new RequestParametersError(errors);
  }
  return req.body;
}

module.exports = {
  validateCreateClock,
};
