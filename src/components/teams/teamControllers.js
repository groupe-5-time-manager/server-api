const errors = require('../../errors');
const database = require('../../database');
const config = require('../../config');
const uuid = require('uuid/v4');

async function teamMiddleware(req, res, next) {
  try {
    const team = await database.team.findByPk(req.params.id);
    if (!team) {
      throw new errors.NotFoundError('TEAM01', 'team')
    }
    if (team.managerId != res.locals.user.id && res.locals.user.roleId != 1)
      throw new errors.AuthError('Your role does not allow you to access this resource');
    res.locals.team = team;
    return next();
  } catch (err) {
    return next(err);
  }
}

async function getTeams(params) {
  try {
    const teams = await database.team.findAll();
    return teams;
  } catch (err) {
    throw err;
  }
}

async function createTeam(name, managerId, usersIds) {
  try {
    const team = await database.team.create({ id: uuid(), name, managerId });
    await database.user.update({ roleId: 2 }, { where: { id: managerId }});
    await team.addUsers(usersIds);
    return team;
  } catch (err) {
    throw err;
  }
}

async function getTeam({ id }) {
  try {
    const team = await database.team.findByPk(id)
    return team;
  } catch (err) {
    throw err;
  }
}

async function addUsers({ users }, team) {
  try {
    await team.addUsers(users);
    return getTeam(team.id);
  } catch (err) {
    console.error('Error adding users to team', err)
    throw err;
  }
}

async function getTeamUsers({ id }) {
  try {
    const team = await database.team.findByPk(id)
    const users = await team.getUsers();
    return users;
  } catch (err) {
    throw err;
  }
}

async function removeUser(team, userId) {
  try {
    await team.removeUser(userId);
    return null;
  } catch (err) {
    throw err;
  }
}

module.exports = {
  teamMiddleware,
  getTeams,
  createTeam,
  getTeam,
  addUsers,
  getTeamUsers,
  removeUser,
};
