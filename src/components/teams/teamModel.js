const DataTypes = require('sequelize');

const name = 'team';

const fields = {
  id: {
    notEmpty: true,
    type: DataTypes.STRING,
    primaryKey: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
};

module.exports = { name, fields };
