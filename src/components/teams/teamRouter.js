const express = require('express');
const httpStatus = require('http-status-codes');
const uuid = require('uuid/v4');

const errors = require('../../errors');

const teamControllers = require('./teamControllers');
const teamValidators = require('./teamValidators');

const { adminMiddleware } = require('../auth/authControllers');

const router = express.Router();

router.get('/', adminMiddleware, async (req, res, next) => {
  try {
    const result = await teamControllers.getTeams();
    return res.status(httpStatus.OK).send(result);
  } catch (err) {
    return next(err);
  }
});

router.post('/', adminMiddleware, async (req, res, next) => {
  try {
    const team = await teamControllers.createTeam(req.body.name, req.body.managerId, req.body.usersIds);
    return res.status(httpStatus.OK).send(team);
  } catch (err) {
    return next(err);
  }
});

router.get('/:id', teamControllers.teamMiddleware, async (req, res, next) => {
  try {
    const team = await teamControllers.getTeam(res.locals.team);
    return res.status(httpStatus.OK).send(team);
  } catch (err) {
    return next(err);
  }
});

router.get('/:id/users', teamControllers.teamMiddleware, async (req, res, next) => {
  try {
    const users = await teamControllers.getTeamUsers(res.locals.team);
    return res.status(httpStatus.OK).send(users);
  } catch (err) {
    return next(err);
  }
});

router.post('/:id/users', teamControllers.teamMiddleware, async (req, res, next) => {
  try {
    const params = teamValidators.validateAddUsers(req);
    const result = await teamControllers.addUsers(params, res.locals.team);
    const users = await teamControllers.getTeamUsers(res.locals.team);
    return res.status(httpStatus.OK).send(users);
  } catch (err) {
    return next(err);
  }
});

router.delete('/:id/users/:userId', teamControllers.teamMiddleware, async (req, res, next) => {
  try {
    const result = await teamControllers.removeUser(res.locals.team, req.params.userId);
    const users = await teamControllers.getTeamUsers(res.locals.team);
    return res.status(httpStatus.OK).send(users);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
