const validate = require('validate.js');
const { RequestParametersError } = require('../../errors');

const addUsersConstraints = {
  users: {
    presence: { allowEmpty: true }
  },
};

function validateAddUsers(req) {
  const errors = validate(req.body, addUsersConstraints);
  if (errors) {
    throw new RequestParametersError(errors);
  }
  return req.body;
}

// const udpateUserConstraints = {
//   username: {
//     type: 'string',
//     presence: { allowEmpty: true }
//   },
//   firstname: {
//     type: 'string',
//     presence: { allowEmpty: true }
//   },
//   lastname: {
//     type: 'string',
//     presence: { allowEmpty: true }
//   },
//   email: {
//     type: 'string',
//     presence: { allowEmpty: true }
//   },
//   password: {
//     type: 'string',
//     presence: { allowEmpty: true }
//   },
// };
//
// function validateUpdateUser(req) {
//   const errors = validate(req.body, udpateUserConstraints);
//   if (errors) {
//     throw new RequestParametersError(errors);
//   }
//   return req.body;
// }

module.exports = {
  validateAddUsers,
  // validateUpdateUser,
};
