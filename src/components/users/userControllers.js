const errors = require('../../errors');
const database = require('../../database');
const config = require('../../config');

async function getAllUsers() {
  try {
    const users = await database.user.findAll();
    return users.map(({id, firstname, lastname, username, email, roleId}) =>
      ({id, firstname, lastname, username, email, roleId}));
  } catch (err) {
    throw err;
  }
}

async function getUser(email, username) {
  try {
    let user = await database.user.findOne({where: { email, username }});
    if (!user) {
      throw new errors.NotFoundError('USER03', 'User');
    }
    user = (({id, firstname, lastname, username, email, roleId}) =>
    ({id, firstname, lastname, username, email, roleId}))(user);
    return user;
  } catch (err) {
    throw err;
  }
}

async function getUserById(userId) {
  try {
    let user = await database.user.findByPk(userId);
    user = (({id, firstname, lastname, username, email, roleId}) =>
    ({id, firstname, lastname, username, email, roleId}))(user);
    if (user.roleId == 2) {
      user.team = await database.team.findOne({where: { managerId: user.id }});
    } else if (user.roleId == 3) {
      user.teams = await user.getTeams();
      user.teams = user.teams.map(({ id, name }) => { id, name });
    }
    return user;
  } catch (err) {
    throw new errors.DatabaseFindError('User');
  }
}

async function createUser(params) {
  try {
    const user = await database.user.create(params);
    return user;
  } catch (err) {
    console.error('Error creating user', err);
    throw new errors.DatabaseCreationError('USER02', 'User');
  }
}

async function updateUser(id, params) {
  try {
    const result = await database.user.update(params, {where: { id }});
    return result;
  } catch (err) {
    console.error('Error updating user', err);
    throw new errors.DatabaseUpdateError('USER02', 'User');
  }
}

async function deleteUser(id) {
  try {
    const result = await database.user.destroy({where: {id}});
    return result;
  } catch (err) {
    console.error('Error deleting user', err);
    throw new errors.DatabaseDeleteError('USER02', 'User');
  }
}

module.exports = {
  getAllUsers,
  getUser,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
};
