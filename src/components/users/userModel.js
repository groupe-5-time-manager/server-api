const DataTypes = require('sequelize');

const name = 'user';

const fields = {
  id: {
    notEmpty: true,
    type: DataTypes.STRING,
    primaryKey: true,
  },
  firstname: {
    type: DataTypes.STRING,
    notEmpty: true,
  },
  lastname: {
    type: DataTypes.STRING,
    notEmpty: true,
  },
  email: {
    type: DataTypes.STRING,
    validate: {
      isEmail: true,
    },
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
};

module.exports = { name, fields };
