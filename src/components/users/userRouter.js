const express = require('express');
const httpStatus = require('http-status-codes');
const uuid = require('uuid/v4');

const errors = require('../../errors');

const userControllers = require('./userControllers');
const userValidators = require('./userValidators');

const router = express.Router();

/**
 * @api {get} /api/users Get one User by his email and his username
 * @apiName GetUser
 * @apiGroup User
 *
 * @apiParam {String} email of user
 * @apiParam {String} username of user
 *
 * @apiSuccess {Object[]} users Users list
 * @apiSuccess {String} user.id User id
 * @apiSuccess {String} user.firstname User firstname
 * @apiSuccess {String} user.lastname User lastname
 * @apiSuccess {String} user.username User username
 * @apiSuccess {String} user.email User email
 * @apiSuccess {String} user.role User role
 */
router.get('/', async (req, res, next) => {
  try {
    const email = req.query.email
    const username = req.query.username
    if (!email || !username) {
      const users = await userControllers.getAllUsers();
      return res.status(httpStatus.OK).send(users);
    }
    const result = await userControllers.getUser(email, username);
    return res.status(httpStatus.OK).send(result);
  } catch (err) {
    return next(err);
  }
});

/**
 * @api {get} /api/users/:userId Get one User by his id
 * @apiName GetUserById
 * @apiGroup User
 *
 * @apiParam {String} userId User id
 *
 * @apiSuccess {String} id User id
 * @apiSuccess {String} firstname User firstname
 * @apiSuccess {String} lastname User lastname
 * @apiSuccess {String} username User username
 * @apiSuccess {String} email User email
 * @apiSuccess {String} role User role
 */
router.get('/:userId', async (req, res, next) => {
  try {
    const user = await userControllers.getUserById(res.locals.user.id);
    return res.status(httpStatus.OK).send(user);
  } catch (err) {
    return next(err);
  }
});

/**
 * @api {post} /api/users Create one User
 * @apiName CreateUser
 * @apiGroup User
 *
 * @apiParam None
 *
 * @apiSuccess {String} id User id
 * @apiSuccess {String} firstname User firstname
 * @apiSuccess {String} lastname User lastname
 * @apiSuccess {String} username User username
 * @apiSuccess {String} email User email
 * @apiSuccess {String} role User role
*/
router.post('/', async (req, res, next) => {
  try {
    const params = userValidators.validateCreateUser(req);
    const result = await userControllers.createUser({
      ...params,
      id: uuid(),
      role: 1
    });
    return res.status(httpStatus.CREATED).send(result);
  } catch (err) {
    return next(err);
  }
});

/**
 * @api {put} /api/users/:userId Update one User by his id
 * @apiName UpdateUser
 * @apiGroup User
 *
 * @apiParam {Number} id User unique ID
 *
 * @apiSuccess {String} id User id
 * @apiSuccess {String} firstname User firstname
 * @apiSuccess {String} lastname User lastname
 * @apiSuccess {String} username User username
 * @apiSuccess {String} email User email
 * @apiSuccess {String} role User role
 */
router.put('/:userId', async (req, res, next) => {
  try {
    if (Object.keys(req.body).length == 0)
      throw new errors.MissingParametersError('USER09');
    req.body = { ...res.locals.user.dataValues, ...req.body }
    const params = userValidators.validateUpdateUser(req);
    const result = await userControllers.updateUser(res.locals.user.id, params);
    const user = await userControllers.getUserById(res.locals.user.id);
    return res.status(httpStatus.OK).send(user);
  } catch (err) {
    return next(err);
  }
});

/**
 * @api {delete} /api/users/:userId Delete one User by his id
 * @apiName DeleteUser
 * @apiGroup User
 *
 * @apiParam {Number} id User unique ID
 *
 * @apiSuccess {Object[]} users Remaining users list
 * @apiSuccess {String} user.id User id
 * @apiSuccess {String} user.firstname User firstname
 * @apiSuccess {String} user.lastname User lastname
 * @apiSuccess {String} user.username User username
 * @apiSuccess {String} user.email User email
 * @apiSuccess {String} user.role User role
 */
router.delete('/:userId', async (req, res, next) => {
  try {
    const result = await userControllers.deleteUser(req.params.userId);
    const users = await userControllers.getAllUsers();
    return res.status(httpStatus.OK).send(users);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
