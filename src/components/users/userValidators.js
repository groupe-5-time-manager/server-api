const validate = require('validate.js');
const { RequestParametersError } = require('../../errors');

const createUserConstraints = {
  // username: {
  //   type: 'string',
  //   presence: { allowEmpty: true }
  // },
  firstname: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  lastname: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  email: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  password: {
    type: 'string',
    presence: { allowEmpty: true }
  },
};

function validateCreateUser(req) {
  const errors = validate(req.body, createUserConstraints);
  if (errors) {
    throw new RequestParametersError(errors);
  }
  return req.body;
}

const udpateUserConstraints = {
  // username: {
  //   type: 'string',
  //   presence: { allowEmpty: true }
  // },
  firstname: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  lastname: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  email: {
    type: 'string',
    presence: { allowEmpty: true }
  },
  password: {
    type: 'string',
    presence: { allowEmpty: true }
  },
};

function validateUpdateUser(req) {
  const errors = validate(req.body, udpateUserConstraints);
  if (errors) {
    throw new RequestParametersError(errors);
  }
  return req.body;
}

module.exports = {
  validateCreateUser,
  validateUpdateUser,
};
