const { Op } = require('sequelize');

const uuid = require('uuid/v4');
const errors = require('../../errors');
const database = require('../../database');

async function workingTimeMiddleware(req, res, next) {
  try {
    const workingTime = await database.workingTime.findByPk(req.params.workingTimeId);
    if (!workingTime) {
        throw new errors.NotFoundError('WT01', 'WorkingTime');
    }
    res.locals.workingTime = workingTime;
    return next();
  } catch (err) {
    return next(err);
  }
}

async function getAllWorkingTimes() {
    try {
        const workingTimes = await database.workingTime.findAll();
        return workingTimes.map(({id, start, end}) => ({id, start, end}));
    } catch (err) {
        throw err;
    }
}

async function getAllWorkingTimesbyUserId(userId) {
  try {
      const workingTimes = await database.workingTime.findAll({ where: { userId } });
      return workingTimes.map(({id, start, end}) => ({id, start, end}));
  } catch (err) {
      throw err;
  }
}

async function getWorkingTimesByRange(userId, start, end) {
  try {
    const workingTimes = await database.workingTime.findAll({where: { userId, start: {[Op.gte]: start}, end: {[Op.lte]: end} }});
    if (!workingTimes) {
      throw new errors.NotFoundError('WT02', 'WorkingTime');
    }
    return workingTimes.map(({id, start, end}) => ({id, start, end}));
  } catch (err) {
    console.error('Error in getWorkingTime', err)
    throw new errors.DatabaseFindError('WorkingTime');
  }
}

async function getWorkingTimeById(id) {
  try {
    const workingTime = await database.workingTime.findOne({where: { id }});

    if (!workingTime) {
        throw new errors.NotFoundError('WT03', 'WorkingTime');
    }
    return workingTime;
  } catch (err) {
    console.error('Error in getWorkingTimeById', err)
    throw new errors.DatabaseFindError('WorkingTime');
  }
}

async function createWorkingTime(userId, params) {
    if (params.start >= params.end) {
      throw new errors.TimeParameterError('WT04', 'Start must be before end')
    }
    try {
        return await database.workingTime.create({ id: uuid(), ...params, userId });
    } catch (err) {
        console.error('Error creating workingTime', err);
        throw new errors.DatabaseCreationError('WorkingTimes');
    }
}

async function updateWorkingTime(id, params) {
    try {
        return await database.workingTime.update(params, { where: { id } });
    } catch (err) {
        console.error('Error updating workingTime', err);
        throw new errors.DatabaseUpdateError('WorkingTimes');
    }
}

async function deleteWorkingTime(id) {
    try {
        return await database.workingTime.destroy({ where: { id } });
    } catch (err) {
        console.error('Error deleting workingTime', err);
        throw new errors.DatabaseDeleteError('WorkingTimes');
    }
}

module.exports = {
    workingTimeMiddleware,
    getAllWorkingTimes,
    getAllWorkingTimesbyUserId,
    getWorkingTimesByRange,
    getWorkingTimeById,
    createWorkingTime,
    updateWorkingTime,
    deleteWorkingTime
};
