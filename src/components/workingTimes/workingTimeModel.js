const DataTypes = require('sequelize');

const name = 'workingTime';

const fields = {
    id: {
        notEmpty: true,
        type: DataTypes.STRING,
        primaryKey: true,
    },
    start: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    end: {
        type: DataTypes.STRING,
        allowNull: false,
    }
};

module.exports = { name, fields };
