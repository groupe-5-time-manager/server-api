const express = require('express');
const httpStatus = require('http-status-codes');

const errors = require('../../errors');
const workingTimeControllers = require('./workingTimeControllers');
const workingTimeValidators = require('./workingTimeValidators');
const { workingTimeMiddleware } = workingTimeControllers;

const router = express.Router();

/**
 * @api {get} /api/workingtimes Get all WorkingTimes by userID, start and end
 * @apiName GetWorkingTimes
 * @apiGroup WorkingTime
 *
 * @apiParam {String} user_id of user
 * @apiParam {String} start of workingTime
 * @apiParam {String} end of workingTime
 *
 * @apiSuccess {Object[]} workingTimes WorkingTime list
 * @apiSuccess {String} workingTimes.id WorkingTime id
 * @apiSuccess {String} workingTimes.start WorkingTime start
 * @apiSuccess {String} workingTimes.end WorkingTime end
 * @apiSuccess {String} workingTimes.user_id WorkingTime user_id
 */
router.get('/:userId', async (req, res, next) => {
  try {
    const { start, end } = workingTimeValidators.validateGetWorkingTimes(req);
    if (!start || !end) {
      const result = await workingTimeControllers.getAllWorkingTimesbyUserId(res.locals.user.id)
      return res.status(httpStatus.OK).send(result)
    }
    const result = await workingTimeControllers.getWorkingTimesByRange(res.locals.user.id, start, end);
    return res.status(httpStatus.OK).send(result);
  } catch (err) {
    return next(err);
  }
});

/**
 * @api {get} /api/workingtimes/:userId/:workingTimeId Get one WorkingTime by userID and workingTimeId
 * @apiName GetWorkingTimeById
 * @apiGroup WorkingTime
 *
 * @apiParam {String} user_id of user
 * @apiParam {String} id WorkingTime id
 *
 * @apiSuccess {String} id WorkingTime id
 * @apiSuccess {String} start WorkingTime firstname
 * @apiSuccess {String} end User lastname
 */
router.get('/:userId/:workingTimeId', workingTimeMiddleware, async (req, res, next) => {
    try {
        const workingTime = await workingTimeControllers.getWorkingTimeById(res.locals.workingTime.id);
        return res.status(httpStatus.OK).send(workingTime);
    } catch (error) {
        return next(error);
    }
});

/**
 * @api {post} /api/workingtimes/:userId Create one WorkingTime with userID
 * @apiName CreateWorkingTime
 * @apiGroup WorkingTime
 *
 * @apiParam None
 *
 * @apiSuccess {String} id WorkingTime id
 * @apiSuccess {String} start WorkingTime firstname
 * @apiSuccess {String} end User lastname
 */
router.post('/:userId', async (req, res, next) => {
    try {
        const params = workingTimeValidators.validateCreateWorkingTime(req);
        const result = await workingTimeControllers.createWorkingTime(res.locals.user.id, params);
        return res.status(httpStatus.CREATED).send(result);
    } catch (err) {
        return next(err);
    }
});

/**
 * @api {put} /api/workingtimes/:workingTimeId Update one WorkingTime with workingTimeId
 * @apiName UpdateWorkingTime
 * @apiGroup WorkingTime
 *
 * @apiParam {Number} id WorkingTime unique ID
 *
 * @apiSuccess {String} id WorkingTime id
 * @apiSuccess {String} start WorkingTime firstname
 * @apiSuccess {String} end User lastname
 */
router.put('/:workingTimeId', workingTimeMiddleware, async (req, res, next) => {
  try {
      if (Object.keys(req.body).length == 0)
        throw new errors.MissingParametersError('WT09');
      req.body = { ...res.locals.workingTime.dataValues, ...req.body }
      const params = workingTimeValidators.validateUpdateWorkingTime(req);
      const result = await workingTimeControllers.updateWorkingTime(res.locals.workingTime.id, params);
      const workingTime = await workingTimeControllers.getWorkingTimeById(res.locals.workingTime.id)
      return res.status(httpStatus.OK).send(workingTime);
  } catch (err) {
      return next(err);
  }
});

/**
 * @api {delete} /api/workingtimes/:workingTimeId Delete one WorkingTime with workingTimeId
 * @apiName DeleteWorkingTime
 * @apiGroup WorkingTime
 *
 * @apiParam {Number} id WorkingTime unique ID
 *
 * @apiSuccess {String} id WorkingTime id
 * @apiSuccess {String} start WorkingTime firstname
 * @apiSuccess {String} end User lastname
 */
router.delete('/:workingTimeId', workingTimeMiddleware, async (req, res, next) => {
    try {
        await workingTimeControllers.deleteWorkingTime(res.locals.workingTime.id)
        const workingTimes = await workingTimeControllers.getAllWorkingTimes();
        return res.status(httpStatus.OK).send(workingTimes);
    } catch (err) {
        return next(err);
    }
});

module.exports = router;
