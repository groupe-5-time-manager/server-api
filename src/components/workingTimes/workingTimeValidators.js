const validate = require('validate.js');
const { RequestParametersError } = require('../../errors');

getWorkingTimesConstraints = {
    start: {
        type: 'string',
        presence: { allowEmpty: true }
    },
    end: {
        type: 'string',
        presence: { allowEmpty: true }
    }
}

function validateGetWorkingTimes(req) {
  const errors = validate(req.query, getWorkingTimesConstraints)
  if (errors) {
      throw new RequestParametersError(errors);
  }
  return req.query;
}

const createWorkingTimeConstraints = {
    start: {
        type: 'string',
        presence: { allowEmpty: false }
    },
    end: {
        type: 'string',
        presence: { allowEmpty: false }
    }
};

function validateCreateWorkingTime(req) {
    const errors = validate(req.body, createWorkingTimeConstraints);

    if (errors) {
        throw new RequestParametersError(errors);
    }
    return req.body;
}

const updateWorkingTimeConstraints = {
    start: {
        type: 'string',
        presence: { allowEmpty: false }
    },
    end: {
        type: 'string',
        presence: { allowEmpty: false }
    }
};

function validateUpdateWorkingTime(req) {
    const errors = validate(req.body, updateWorkingTimeConstraints);

    if (errors) {
        throw new RequestParametersError(errors);
    }
    return req.body;
}

module.exports = {
  validateGetWorkingTimes,
  validateCreateWorkingTime,
  validateUpdateWorkingTime,
};
