const fs = require('fs');
const crypto = require('crypto');

const modulusLength = 2048;
const publicKeyEncoding = {
  type: 'spki',
  format: 'pem',
};
const privateKeyEncoding = {
  type: 'pkcs8',
  format: 'pem',
};

const { privateKey, publicKey } = crypto.generateKeyPairSync('rsa', { modulusLength, publicKeyEncoding, privateKeyEncoding });

const config = {
  port: process.env.APP_PORT,
  dbUser: process.env.DB_USER,
  dbPassword: process.env.DB_PASSWORD,
  dbHost: process.env.DB_HOST,
  dbPort: process.env.DB_PORT,
  dbName: process.env.DB_NAME,
  salt: 'APykw4eHbFpUfwrmKbQfeKT8VdBapmXbtEh7hX8SF6q9u7nFVXV69EsJ7By7VeeF',
  saltRounds: 10,
  authPublicKey: publicKey,
  authPrivateKey: privateKey,
}

module.exports = config;
