const Sequelize = require('sequelize');
const uuid = require('uuid/v4');
const bcrypt = require('bcrypt');

const user = require('./components/users/userModel');
const clock = require('./components/clocks/clockModel');
const workingTime = require('./components/workingTimes/workingTimeModel');
const role = require('./components/auth/roleModel');
const team = require('./components/teams/teamModel');

const config = require('./config');

const databasePath = `mysql://${config.dbUser}:${config.dbPassword}@${config.dbHost}:${config.dbPort}/${config.dbName}`;
const options = { logging: false };
const sequelize = new Sequelize(databasePath, options);
const database = {};

function initializeModels(sequelize, database) {
  const modelOptions = {
    underscored: false,
    freezeTableName: true,
  };
  database.user = sequelize.define(user.name, user.fields, modelOptions);
  database.role = sequelize.define(role.name, role.fields, modelOptions);
  database.clock = sequelize.define(clock.name, clock.fields, modelOptions);
  database.workingTime = sequelize.define(workingTime.name, workingTime.fields, modelOptions);
  database.team = sequelize.define(team.name, team.fields, modelOptions);

  database.role.hasMany(database.user, { foreignKey: { allowNull: false } });
  database.user.belongsTo(database.role, { foreignKey: { allowNull: false } });

  database.user.hasMany(database.clock, { foreignKey: { allowNull: false } });
  database.clock.belongsTo(database.user, { foreignKey: { allowNull: false } });

  database.user.hasMany(database.workingTime, { foreignKey: { allowNull: false } });
  database.workingTime.belongsTo(database.user, { foreignKey: { allowNull: false } });

  database.team.belongsTo(database.user, { foreignKey: { allowNull: false, name: 'managerId' } });

  database.user.belongsToMany(database.team, { through: 'user_team' });
  database.team.belongsToMany(database.user, { through: 'user_team' });
}

async function connect() {
  let connected = false;

  while (!connected) {
    console.log('connecting to database...');
    try {
      // eslint-disable-next-line no-await-in-loop
      await sequelize.sync();
      connected = true;
    } catch (error) {
      console.error(`failed to connect to database (${error})`);
      // eslint-disable-next-line no-await-in-loop
      await new Promise((resolve) => setTimeout(resolve, 1000));
    }
  }
  console.log('database connected.');
}

async function initializeRoles() {
  try {
    const roles = await database.role.findAll();
    if (roles.length == role.roles.length)
      return
    const promises = role.roles.map(async (r) => {await database.role.create({ label: r })});
    await Promise.all(promises);
  } catch (err) {
    console.error('Error initializing roles', err);
  }
}

async function initializeAdmin() {
  try {
    const { id: adminId } = await database.role.findOne({where: { label: 'admin' }});
    const admin = await database.user.findOne({ where: { roleId: adminId} });
    if (admin)
      return
    const password = bcrypt.hashSync('admin' + config.salt, config.saltRounds);
    await database.user.create({id: uuid(), firstname: 'Charles', lastname: 'Montgomery Burns', email: 'admin@admin.com', password, roleId: adminId, teamId: null});
  } catch (err) {
    console.error('Error initializing admin', err);
    throw err;
  }
}

async function initializeManager() {
  try {
    const { id: managerId } = await database.role.findOne({where: { label: 'manager' }});
    const manager = await database.user.findOne({ where: { roleId: managerId} });
    if (manager) return

    const password = bcrypt.hashSync('manager' + config.salt, config.saltRounds);
    await database.user.create({id: uuid(), firstname: 'Waylon', lastname: 'Smithers', email: 'manager@manager.com', password, roleId: managerId, teamId: null});
  } catch (err) {
    console.error('Error initializing manager', err);
    throw err;
  }
}

async function initializeClockData() {
  try {
    const manager = await database.user.findOne({ where: { email: 'manager@manager.com'} });
    const c = await database.clock.findAll();
    if (c.length > 0)
      return
    const clocks = require('../data_test/clocks.json')
    for (const entry of clocks) {
      try {
        await database.clock.create({
          id: uuid(),
          time: entry.time,
          status: entry.status,
          userId: manager.id
        });
        if (!entry.status) {
          const lastClocks = await database.clock.findAll({ limit: 2, order: [['time', 'DESC']] });
          const start = lastClocks[1].time;
          const end = entry.time;
          await database.workingTime.create({ id: uuid(), start, end, userId: manager.id });
        }

      } catch (error) {
        console.error('Error inserting clock')
        throw error
      }
    }
  } catch (e) {
    console.error('Error initializing data', e);
    throw e;
  }
}

async function initializeTeamData() {
  try {
    const manager = await database.user.findOne({ where: { email: 'manager@manager.com'} });

    // CREATE USER
    const users = require('../data_test/users.json')
    let usersIds = []
    for (const iterator of users) {
      try {
        const user = await database.user.create({
          firstname: iterator.firstname,
          lastname: iterator.lastname,
          password: bcrypt.hashSync(iterator.password + config.salt, config.saltRounds),
          email: iterator.email,
          id: uuid(),
          roleId: 3
        });
        usersIds.push(user.id)
      } catch (error) {
        throw error
      }
    }

    // CREATE TEAM
    const teams = require('../data_test/teams.json')
    for (const iterator of teams) {
      try {
        const team = await database.team.create({ id: uuid(), name: iterator.name, managerId: manager.id });
        await database.user.update({ roleId: 2 }, { where: { id: manager.id }});
        // ADD USER TO TEAM
        try {
          await team.addUsers(usersIds);
        } catch (err) {
          console.error('Error adding users to team', err)
          throw err;
        }
      } catch (error) {
        throw error
      }
    }
  } catch (err) {
    throw err;
  }

}


async function initialize() {
  initializeModels(sequelize, database)
  await connect();
  await initializeRoles();
  await initializeAdmin();
  await initializeManager();
  await initializeClockData();
  await initializeTeamData();
}

database.initialize = initialize;
database.DataTypes = Sequelize.DataTypes;

module.exports = database;
