const httpStatus = require('http-status-codes');

const errorMiddleware = (error, _, res, next) => {
  if (error.status >= 500 || error.shouldBePrinted) {
    console.error(
      `${error.stack}`,
    );
  }
  if (!error.status) {
    console.error(error.stack);
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).send({ message: 'Internal error' });
  }
  console.log(`Request failed with status code ${error.code}: ${error.message}`);
  return res.status(error.status).send({ message: error.message });
};

class GandalfError extends Error {
  constructor(code, status, message, shouldBePrinted = true) {
    if (code === undefined || message === undefined) {
      console.error('Undefined code or message');
    }
    super();
    this.code = code;
    this.status = status;
    this.message = message;
    this.shouldBePrinted = shouldBePrinted;
  }
}

class RequestParametersError extends GandalfError {
  constructor(errors) {
    super('1', httpStatus.BAD_REQUEST, '', false);
    this.buildMessage(errors);
  }

  buildMessage(errors) {
    let output = 'The following fields are not valid :\n';
    Object.keys(errors).forEach((property) => {
      const messages = errors[property];
      messages.forEach((message) => {
        output += `${message}\n`;
      });
    });
    this.message = output;
  }
}

class MissingParametersError extends GandalfError {
  constructor(code) {
    super(code, httpStatus.BAD_REQUEST, 'Missing parameters');
  }
}

class TimeParameterError extends GandalfError {
  constructor(code, message) {
    super(code, httpStatus.BAD_REQUEST, message);
  }
}

class AuthError extends GandalfError {
  constructor(message) {
    super('AUTH02', httpStatus.UNAUTHORIZED, message);
  }
}

class UserAlreadyExistsError extends GandalfError {
  constructor(email) {
    super('AUTH01', httpStatus.BAD_REQUEST, `User with email ${email} already exists`);
  }
}

class InternalError extends GandalfError {
  constructor(code, message) {
    super(code, httpStatus.INTERNAL_SERVER_ERROR, message);
  }
}

class DatabaseError extends InternalError {
  constructor(message) {
    super('DB01', message);
  }
}

class DatabaseFindError extends DatabaseError {
  constructor(table) {
    super(`Error finding ${table}`);
  }
}

class DatabaseCreationError extends DatabaseError {
  constructor(table) {
    super(`Error creating ${table}`);
  }
}

class DatabaseUpdateError extends DatabaseError {
  constructor(table) {
    super(`Error updating ${table}`);
  }
}

class DatabaseDeleteError extends DatabaseError {
  constructor(table) {
    super(`Error deleting ${table}`);
  }
}

class NotFoundError extends GandalfError {
  constructor(code, entity) {
    super(code, httpStatus.NOT_FOUND, `${entity} not found`);
  }
}

class ForbiddenError extends GandalfError {
  constructor(code) {
    super(code, httpStatus.FORBIDDEN, `You have insufficient rights`);
  }
}

module.exports = {
  errorMiddleware,
  GandalfError,
  RequestParametersError,
  AuthError,
  MissingParametersError,
  TimeParameterError,
  UserAlreadyExistsError,
  InternalError,
  DatabaseError,
  DatabaseFindError,
  DatabaseCreationError,
  DatabaseUpdateError,
  DatabaseDeleteError,
  NotFoundError,
  ForbiddenError,
};
