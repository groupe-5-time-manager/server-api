const express = require('express');

const authRouter = require('./components/auth/authRouter');
const userRouter = require('./components/users/userRouter');
const clockRouter = require('./components/clocks/clockRouter');
const workingTimeRouter = require('./components/workingTimes/workingTimeRouter');
const teamsRouter = require('./components/teams/teamRouter');

const { userMiddleware, managerMiddleware } = require('./components/auth/authControllers');

const router = express.Router();

router.use('/auth', authRouter);
router.use('/users', userMiddleware, userRouter);
router.use('/clocks/:userId', userMiddleware, clockRouter);
router.use('/workingtimes', userMiddleware, workingTimeRouter);
router.use('/teams', managerMiddleware, teamsRouter);

module.exports = router;
