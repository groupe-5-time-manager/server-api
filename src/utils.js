const jwt = require('jsonwebtoken');

const config = require('./config');
const errors = require('./errors');

const jwtOptions = {
  expiresIn: '30d',
  algorithm: 'RS256',
  issuer: 'gandalf',
};

const authRoles = {
  USER: 1,
  DEV: 2,
  ADMIN: 3,
};

function generateJsonWebToken(aud, sub, payload) {
  const options = { ...jwtOptions };
  options.audience = aud;
  options.subject = sub;

  try {
    return jwt.sign(payload, config.authPrivateKey, options);
  } catch (error) {
    throw new errors.InternalError(
      'INTERNAL01',
      `Cannot sign the json web token: ${error.message}`,
    );
  }
}

function verifyJsonWebToken(token) {
  try {
    return jwt.verify(token, config.authPublicKey, { audience: 'gandalf', algorithms: ['RS256'] });
  } catch (error) {
    throw new errors.AuthError(error.message);
  }
}

module.exports = {
  generateJsonWebToken,
  verifyJsonWebToken
}
