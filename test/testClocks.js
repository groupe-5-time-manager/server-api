var supertest = require("supertest");
var should = require("should");

// This agent refers to PORT where program is runninng.

var server = supertest.agent("http://0.0.0.0:4000");
var id_test = "";
var token = "";

// UNIT test begin

describe("CLOCKS unit tests",function(){

    it("POST create user for clock tests",function(done){

        server
            .post("/api/auth/signup")
            .send({username: "toto", firstname: "toto", lastname: "toto", email: "toto@toto.fr", password:"toto"})
            .expect(201)
            .end(function(err,res){
                // HTTP status should be 201
                console.log(res.body);
                res.status.should.equal(201);
                id_test = res.body.userId;
                token = res.body.xsrfToken;
                done();
            });
    });

    it("POST userID unknown", function(done){
       server
           .post("/api/clocks/123456890")
           .set('X-CSRF-TOKEN', token)
           .expect(404)
           .end(function(err,res){
               res.status.should.equal(404);
               done();
           });
    });

    it("POST userID known but missing info", function(done){
        server
            .post("/api/clocks/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .expect(400)
            .end(function(err,res){
                res.status.should.equal(400);
                //res.body.should.equal("The following fields are not valid :\nTime can't be blank\nStatus can't be blank\n");
                done();
            });
    });

    it("POST userID known but wrong info", function(done){
        server
            .post("/api/clocks/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .send({time: "abc", status: "abc"})
            .expect(400)
            .end(function(err,res){
                res.status.should.equal(400);
                //res.body.should.equal("The following fields are not valid :\nTime can't be blank\nStatus can't be blank\n");
                done();
            });
    });

    it("POST userID known and first clock status false", function(done){
        server
            .post("/api/clocks/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .send({time: "2019-09-10T08:00:00.000Z", status: false})
            .expect(400)
            .end(function(err,res){
                res.status.should.equal(400);
                done();
            });
    });
    
    it("POST userID known and good info", function(done){
      server
          .post("/api/clocks/" + id_test)
          .set('X-CSRF-TOKEN', token)
          .send({time: "2019-09-10T08:00:00.000Z", status: true})
          .expect(201)
          .end(function(err,res){
              res.status.should.equal(201);
              done();
          });
    });

    it("POST userID known and double clock status true submitted", function(done){
      server
          .post("/api/clocks/" + id_test)
          .set('X-CSRF-TOKEN', token)
          .send({time: "2019-09-10T12:00:00.000Z", status: true})
          .expect(400)
          .end(function(err,res){
              res.status.should.equal(400);
              done();
          });
    });
  
    it("POST userID known and clock out that create new working time", function(done){
      server
          .post("/api/clocks/" + id_test)
          .set('X-CSRF-TOKEN', token)
          .send({time: "2019-09-10T12:00:00.000Z", status: false})
          .expect(201)
          .end(function(err,res){
              res.status.should.equal(201);
              done();
          });
    });

    it("GET userID unknown", function(done){
        server
            .get("/api/clocks/123456890")
            .set('X-CSRF-TOKEN', token)
            .expect(404)
            .end(function(err,res){
                res.status.should.equal(404);
                done();
            });
    });

    it("GET userID known", function(done){
        server
            .get("/api/clocks/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                done();
            });
    });

    it("DELETE test user", function(done){
        server
            .del("/api/users/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                done();
            });
    });

});
