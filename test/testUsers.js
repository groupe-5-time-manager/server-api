var supertest = require("supertest");
var should = require("should");

// This agent refers to PORT where program is runninng.

var server = supertest.agent("http://0.0.0.0:4000");
var id_toto = "";
var token = "";

// UNIT test begin

describe("USERS unit tests",function(){

    it("POST new user toto",function(done){

        server
            .post("/api/auth/signup")
            .send({username: "toto", firstname: "toto", lastname: "toto", email: "toto@toto.fr", password:"toto"})
            .expect(201)
            .end(function(err,res){
                // HTTP status should be 201
                res.status.should.equal(201);
                id_toto = res.body.id;
                token = res.body.xsrfToken;
                done();
            });
    });

    it("GET non-existing user",function(done){

        server
            .get("/api/users?username=toto&email=toto@toto.toto")
            //.expect("Content-type",/json/)
            .set('X-CSRF-TOKEN', token)
            .expect(404) // THis is HTTP response
            .end(function(err,res){
                // HTTP status should be 404
                res.status.should.equal(404);
                done();
            });
    });

    it("POST new user no info",function(done){

        server
            .post("/api/users")
            .set('X-CSRF-TOKEN', token)
            .expect(400)
            .end(function(err,res){
                // HTTP status should be 400
                res.status.should.equal(400);
                res.body.message.should.equal("The following fields are not valid :\nUsername can't be blank\nFirstname can't be blank\nLastname can't be blank\nEmail can't be blank\nPassword can't be blank\n")
                done();
            });
    });

    it("POST new user toto not all infos",function(done){

        server
            .post("/api/users")
            .set('X-CSRF-TOKEN', token)
            .send({username: "toto"})
            .expect(400)
            .end(function(err,res){
                // HTTP status should be 400
                res.status.should.equal(400);
                res.body.message.should.equal("The following fields are not valid :\nFirstname can't be blank\nLastname can't be blank\nEmail can't be blank\nPassword can't be blank\n")
                done();
            });
    });

    it("POST new user toto all infos",function(done){

        server
            .post("/api/users")
            .set('X-CSRF-TOKEN', token)
            .send({username: "toto", firstname: "toto", lastname: "toto", email: "toto@toto.fr", password:"toto"})
            .expect(201)
            .end(function(err,res){
                // HTTP status should be 201
                res.status.should.equal(201);
                id_toto = res.body.id;
                token = res.body.xsrfToken;
                done();
            });
    });

    it("GET user toto just created",function(done){

        server
            .get("/api/users?username=toto&email=toto@toto.fr")
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function(err,res){
                // HTTP status should be 200
                res.status.should.equal(200);
                done();
            });
    });

    it("PUT no info given",function(done){

        server
            .put("/api/users/" + id_toto)
            .set('X-CSRF-TOKEN', token)
            .expect(400)
            .end(function(err,res){
                // HTTP status should be 400
                res.status.should.equal(400);
                done();
            });
    });

    it("PUT change created user info",function(done){

        server
            .put("/api/users/" + id_toto)
            .set('X-CSRF-TOKEN', token)
            .send({email: "toto@toto.toto"})
            .expect(200)
            .end(function(err,res){
                // HTTP status should be 200
                res.status.should.equal(200);
                done();
            });
    });

    it("GET user with old info",function(done){

        server
            .get("/api/users?username=toto&email=toto@toto.fr")
            //.expect("Content-type",/json/)
            .set('X-CSRF-TOKEN', token)
            .expect(404) // THis is HTTP response
            .end(function(err,res){
                // HTTP status should be 404
                res.status.should.equal(404);
                done();
            });
    });

    it("GET user with new info",function(done){

        server
            .get("/api/users?username=toto&email=toto@toto.toto")
            //.expect("Content-type",/json/)
            .set('X-CSRF-TOKEN', token)
            .expect(200) // THis is HTTP response
            .end(function(err,res){
                // HTTP status should be 200
                res.status.should.equal(200);
                done();
            });
    });

    it("DELETE user toto",function(done){

        server
            .del("/api/users/" + id_toto)
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function(err,res){
                // HTTP status should be 200
                res.status.should.equal(200);
                done();
            });
    });

    it("GET user toto just deleted",function(done){

        server
            .get("/api/users?username=toto&email=toto@toto.toto")
            .set('X-CSRF-TOKEN', token)
            .expect(404)
            .end(function(err,res){
                // HTTP status should be 404
                res.status.should.equal(404);
                done();
            });
    });
});
