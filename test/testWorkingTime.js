var supertest = require("supertest");
var should = require("should");

// This agent refers to PORT where program is runninng.

var server = supertest.agent("http://0.0.0.0:4000");
var id_test = "";
var id_wt = "";
var token = "";

describe("WORKINGTIME unit tests",function() {

    it("POST create user for workingtime tests", function (done) {

        server
            .post("/api/auth/signup")
            .send({username: "toto", firstname: "toto", lastname: "toto", email: "toto@toto.fr", password: "toto"})
            .expect(201)
            .end(function (err, res) {
                // HTTP status should be 201
                res.status.should.equal(201);
                id_test = res.body.id;
                token = res.body.xsrfToken;
                done();
            });
    });

    it("POST with invalid userID and no data", function (done) {

        server
            .post("/api/workingtimes/123456890")
            .set('X-CSRF-TOKEN', token)
            .expect(400)
            .end(function (err, res) {
                // HTTP status should be 404
                res.status.should.equal(404);
                done();
            });
    });

    it("POST with invalid userID", function (done) {

        server
            .post("/api/workingtimes/123456890")
            .set('X-CSRF-TOKEN', token)
            .send({start: "2019-09-10T10:00:00.000Z", end: "2019-09-10T18:00:00.000Z"})
            .expect(404)
            .end(function (err, res) {
                // HTTP status should be 404
                res.status.should.equal(404);
                done();
            });
    });

    it("POST with valid userID but no data", function (done) {

        server
            .post("/api/workingtimes/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .expect(400)
            .end(function (err, res) {
                // HTTP status should be 400
                res.status.should.equal(400);
                done();
            });
    });

    it("POST with valid userID but only start", function (done) {

        server
            .post("/api/workingtimes/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .send({start: "2019-09-10T18:00:00.000Z"})
            .expect(400)
            .end(function (err, res) {
                // HTTP status should be 400
                res.status.should.equal(400);
                done();
            });
    });

    it("POST with valid userID but only end", function (done) {

        server
            .post("/api/workingtimes/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .send({end: "2019-09-10T18:00:00.000Z"})
            .expect(400)
            .end(function (err, res) {
                // HTTP status should be 400
                res.status.should.equal(400);
                done();
            });
    });

    it("POST with valid userID and valid data", function (done) {

        server
            .post("/api/workingtimes/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .send({start: "2019-09-10T10:00:00.000Z", end: "2019-09-10T18:00:00.000Z"})
            .expect(201)
            .end(function (err, res) {
                // HTTP status should be 201
                res.status.should.equal(201);
                id_wt = res.body.id;
                done();
            });
    });

    it("GET from non-existing user", function (done) {

        server
            .get("/api/workingtimes/123456890")
            .set('X-CSRF-TOKEN', token)
            .expect(404)
            .end(function (err, res) {
                // HTTP status should be 404
                res.status.should.equal(404);
                done();
            });
    });

    it("GET from existing user with no filter", function (done) {

        server
            .get("/api/workingtimes/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function (err, res) {
                // HTTP status should be 200
                res.status.should.equal(200);
                done();
            });
    });

    it("GET from existing user with filter for no data", function (done) {

        server
            .get("/api/workingtimes/" + id_test + "?start=2019-09-10T8:00:00.000Z&end=2019-09-10T09:00:00.000Z")
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function (err, res) {
                // HTTP status should be 200
                res.status.should.equal(200);
                // There should be something in the body
                res.body.should.empty();
                done();
            });
    });

    it("GET from existing user with filter for data", function (done) {

        server
            .get("/api/workingtimes/" + id_test + "?start=2019-09-10T09:00:00.000Z&end=2019-09-10T19:00:00.000Z")
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function (err, res) {
                // HTTP status should be 200
                res.status.should.equal(200);
                // There should be something in the body
                res.body[0].start.should.equal("2019-09-10T10:00:00.000Z");
                res.body[0].end.should.equal("2019-09-10T18:00:00.000Z");
                done();
            });
    });

    it("GET from existing user with workingtimeID invalid", function (done) {

        server
            .get("/api/workingtimes/" + id_test + "/12346780")
            .set('X-CSRF-TOKEN', token)
            .expect(404)
            .end(function (err, res) {
                // HTTP status should be 200
                res.status.should.equal(404);
                done();
            });
    });

    it("GET from existing user with workingtimeID valid", function (done) {

        server
            .get("/api/workingtimes/" + id_test + "/" + id_wt)
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function (err, res) {
                // HTTP status should be 200
                res.status.should.equal(200);
                res.body.start.should.equal("2019-09-10T10:00:00.000Z");
                res.body.end.should.equal("2019-09-10T18:00:00.000Z");
                done();
            });
    });

    it("PUT invalid id", function (done) {

        server
            .get("/api/workingtimes/" + id_test + "/1234567890")
            .set('X-CSRF-TOKEN', token)
            .expect(404)
            .end(function (err, res) {
                // HTTP status should be 404
                res.status.should.equal(404);
                done();
            });
    });

    it("PUT valid id but no info to modify", function (done) {

        server
            .put("/api/workingtimes/" + id_wt)
            .set('X-CSRF-TOKEN', token)
            .send({})
            .expect(400)
            .end(function (err, res) {
                // HTTP status should be 400
                res.status.should.equal(400);
                done();
            });
    });

    it("PUT valid id and bad info", function (done) {

        server
            .put("/api/workingtimes/" + id_wt)
            .set('X-CSRF-TOKEN', token)
            .send({start: "toto", end: 4})
            .expect(400)
            .end(function (err, res) {
                // HTTP status should be 400
                res.status.should.equal(400);
                done();
            });
    });

    it("PUT valid id and good info", function (done) {

        server
            .put("/api/workingtimes/" + id_wt)
            .set('X-CSRF-TOKEN', token)
            .send({start: "2019-01-10T09:00:00.000Z", end: "2019-01-10T19:00:00.000Z"})
            .expect(200)
            .end(function (err, res) {
                // HTTP status should be 200
                res.status.should.equal(200);
                done();
            });
    });

    it("GET workingtime modified", function (done) {

        server
            .get("/api/workingtimes/" + id_test + "/" + id_wt)
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function (err, res) {
                // HTTP status should be 200
                res.status.should.equal(200);
                res.body.start.should.equal("2019-01-10T09:00:00.000Z");
                res.body.end.should.equal("2019-01-10T19:00:00.000Z");
                done();
            });
    });

    it("DELETE invalid id", function(done){
        server
            .del("/api/workingtimes/" + "123456890")
            .set('X-CSRF-TOKEN', token)
            .expect(404)
            .end(function(err,res){
                res.status.should.equal(404);
                done();
            });
    });

    it("DELETE valid id", function(done){
        server
            .del("/api/workingtimes/" + id_wt)
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                done();
            });
    });

    it("GET workingtime deleted", function (done) {

        server
            .get("/api/workingtimes/" + id_test + "/" + id_wt)
            .set('X-CSRF-TOKEN', token)
            .expect(404)
            .end(function (err, res) {
                // HTTP status should be 200
                res.status.should.equal(404);
                done();
            });
    });

    it("DELETE test user", function(done){
        server
            .del("/api/users/" + id_test)
            .set('X-CSRF-TOKEN', token)
            .expect(200)
            .end(function(err,res){
                res.status.should.equal(200);
                done();
            });
    });

});
